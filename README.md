django-heroku-addon
===================

A template for a Heroku addon in Python/Django. Assumes some familiarity with the [Heroku addon program](http://addons.heroku.com).

Clone the repo:

    git clone https://bilalaslam@github.com/bilalaslam/django-heroku-addon.git

Install Python requiremets

    pip install -r requirements.txt

Install Kenas (Heroku's testing tool)

    gem install kensa

Generate an initial `addon-manifest.json` file.

    kensa init

Make/note the following changes to `addon-manifest.json` file:

- Update the value of the `id` field to match the name of your addon. In example, `"id": "jeffsaddon"`.

- Update the value of `MYADDON_URL` to match your add-on name (in all CAPS) and add `_URL` to it. If you add-on is named `jeffsaddon` then replace `MYADDON_URL` with `JEFFSADDON_URL`.

- Note both your `password` and `sso_salt` values because you will need them for environment variables coming up.

- Update both `base_url` and `sso_url` in the `test` section to match your local development environment or the server which you will be testing against. For local development, I change it from `http://localhost:4567/heroku/resources` to `http://localhost:8000/heroku/resources`.

You will want to set a few environment variables.

    export DJANGO_SETTINGS_MODULE=guples.settings
    export HEROKU_USERNAME='YOUR-ADDONS-NAME'
    export HEROKU_PASSWORD='YOUR-ADDONS-PASSWORD'
    export HEROKU_SSO_SALT='YOUR-ADDONS-SS0-SALT'
    export PORT=8000

Install Foreman

    gem install foreman

Create your database

    createdb guples_dev

Sync Django with your database

    django-admin.py syncdb --migrate

Load initial data for some plans to test again. You need both `test` and `foo` plans in order for `kensa` to valdiate your addon.

    django-admin.py loaddata initial_data.json

3. kensa test *action* e.g. 

    kensa test provision

With any luck:

Provision - works

Deprovision - works

Plan change - works

SSO - works

4. Push to `addon-manifest.json` to Heroku for testing.

    kensa push

5. Profit
