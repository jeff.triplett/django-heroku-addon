import random
import string

from django.db import models


def _generate_random_string(length):
    return ''.join(random.choice(string.letters + string.digits) for i in xrange(length))


class Guple(models.Model):
    guple_store = models.ForeignKey('GupleStore')
    key = models.TextField(blank=False, null=False)
    value = models.TextField(blank=True, null=False)

    def __str__(self):
        return self.key


class GupleStore(models.Model):
    secret_key = models.CharField(max_length=16)
    plan = models.ForeignKey('Plan')

    def __str__(self):
        return self.plan.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.secret_key = _generate_random_string(16)

        super(GupleStore, self).save(*args, **kwargs)


class Plan(models.Model):
    name = models.CharField(max_length=512)
    max_guples = models.IntegerField(default=10)

    def __str__(self):
        return self.name
