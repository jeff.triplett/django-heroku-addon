from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from reroute import include, patterns, url
from reroute.verbs import verb_url


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', 'guples.views.home'),

    # Heroku-specific routes
    verb_url('POST', r'^heroku/resources$', 'guples.views.heroku_provision'),
    verb_url('DELETE', r'^heroku/resources/(?P<id>[0-9].*)$', 'guples.views.heroku_deprovision'),
    verb_url('PUT', r'^heroku/resources/(?P<id>[0-9].*)$', 'guples.views.heroku_planchange'),
    verb_url('GET', r'^heroku/resources/(?P<id>[0-9].*)$', 'guples.views.heroku_sso_for_resource'),
    verb_url('POST', r'^sso/login$', 'guples.views.heroku_sso'),
    verb_url('GET', r'^heroku/ssolanding$', 'guples.views.heroku_sso_landing'),
) + staticfiles_urlpatterns()
