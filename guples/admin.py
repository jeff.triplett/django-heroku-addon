from django.contrib import admin

from .models import Guple, GupleStore, Plan


class GupleAdmin(admin.ModelAdmin):
    pass


class GupleStoreAdmin(admin.ModelAdmin):
    pass


class PlanAdmin(admin.ModelAdmin):
    list_display = ['name', 'max_guples']


admin.site.register(Guple, GupleAdmin)
admin.site.register(GupleStore, GupleStoreAdmin)
admin.site.register(Plan, PlanAdmin)
